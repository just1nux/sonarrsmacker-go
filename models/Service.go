package models

type Service struct {
	Name 		 string     `json:"Name"`
	Image        string     `json:"Image"`
	Series       []Series   `json:"Series"`
}


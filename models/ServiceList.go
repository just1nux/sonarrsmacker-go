package models

type ServiceList struct {
	Setting       Setting        `json:"Setting"`
	Services	  []Service  `json:"Service"`
}


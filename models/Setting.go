package models

type Setting struct {
	CheckEveryMinutes       int      `json:"CheckEveryMinutes"`
	SonarrURL		        string   `json:"SonarrURL"`
	SonarrToken             string   `json:"SonarrToken"`
}


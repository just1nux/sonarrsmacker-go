package dao

import (
	"bitbucket.org/just1nux/sonarrsmacker-go/models"
	_ "fmt"
	"os"
)

const (
	configFilename = "/var/go/config/config.json"
)

var serviceList models.ServiceList

func GetConfigFromFile(){
	// Get config file from file system
	if _, err := os.Stat(configFilename); err == nil {
		serviceList, err = loadConfig(configFilename)
		if err != nil {
			panic(err)
		}
	} else if os.IsNotExist(err) {
		err := saveConfig(createMockConfig(), configFilename)
		if err != nil {
			panic(err)
		}
		serviceList, err = loadConfig(configFilename)
		if err != nil {
			panic(err)
		}
	} else {
		// file may or may not exist.
	}

	//fmt.Printf("%+v\n", serviceList.Setting.SonarrURL)
	//fmt.Printf("%+v\n", serviceList.Setting.SonarrToken)
	//fmt.Printf("%+v\n", serviceList.Setting.CheckEveryMinutes)
}
package dao

import (
	"bitbucket.org/just1nux/sonarrsmacker-go/models"
	"encoding/json"
	"io/ioutil"
)

func saveConfig(c models.ServiceList, filename string) error {
	bytes, err := json.MarshalIndent(c, "", "  ")
	if err != nil {
		return err
	}

	return ioutil.WriteFile(filename, bytes, 0644)
}

func loadConfig(filename string) (models.ServiceList, error) {
	bytes, err := ioutil.ReadFile(filename)
	if err != nil {
		return models.ServiceList{}, err
	}

	var c models.ServiceList
	err = json.Unmarshal(bytes, &c)
	if err != nil {
		return models.ServiceList{}, err
	}

	return c, nil
}

func createMockConfig() models.ServiceList {
	return models.ServiceList{
		Setting: models.Setting{
			CheckEveryMinutes: 2,
			SonarrURL: "http://192.168.1.101:8989/",
			SonarrToken: "bb1fdb4fd51f4164b21e401d7a59837d",
		},
		Services: []models.Service {
			 {
				Name: "Amazon",
				Image: "amazon",
				Series: []models.Series{
					{
						Name: "All or Nothing",
						SeriesId: "514",
					},
				},
			},
		},
	}
}


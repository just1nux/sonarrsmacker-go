package main

import (
	"bitbucket.org/just1nux/sonarrsmacker-go/dao"
	"bitbucket.org/just1nux/sonarrsmacker-go/routers"
	_ "github.com/denisenkom/go-mssqldb"
)

const (
	basePath = "/api/"
)

func main() {
	dao.GetConfigFromFile()
	routers.StartServer()
}








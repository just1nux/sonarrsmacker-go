package routers

import (
	"bitbucket.org/just1nux/sonarrsmacker-go/smack"
	"fmt"
	_ "fmt"
	"github.com/gorilla/mux"
	"net/http"
)

func StartSmackRouter(r *mux.Router, basePath string) {
	var localPath = "smackrun"
	r.HandleFunc(basePath + localPath, getSmackRunEndPoint).Methods("GET")
	localPath = "smackstart"
	r.HandleFunc(basePath + localPath, getSmackStartEndPoint).Methods("GET")

	fmt.Println("  -- Starting Smack Router")
}

func getSmackRunEndPoint(w http.ResponseWriter, r *http.Request) {
	respondWithJson(w, http.StatusOK, smack.SmackRun)
}

func getSmackStartEndPoint(w http.ResponseWriter, r *http.Request) {

	respondWithJson(w, http.StatusOK, smack.SmackStart)
}
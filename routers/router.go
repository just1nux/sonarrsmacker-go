package routers

import (
	. "encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

const (
	basePath = "/api/"
)

func StartServer() {
	r := mux.NewRouter()
	startRouters(r)
	fmt.Println("Listening on port 3030")

	if err := http.ListenAndServe(":3030", r); err != nil {
		log.Fatal(err)
	}

}

func startRouters(r *mux.Router){
	fmt.Println("<< Start Routers")
	StartSmackRouter(r, basePath)
	fmt.Println(">>")
}

func respondWithError(w http.ResponseWriter, code int, msg string) {
	respondWithJson(w, code, map[string]string{"error": msg})
}

func respondWithJson(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	_, _ = w.Write(response)
}


# Use an official Golang runtime as a parent image using Alpine
FROM golang:1.11

# Get Dependencies
RUN go get -u -v github.com/denisenkom/go-mssqldb github.com/gorilla/mux github.com/jinzhu/gorm github.com/ant0ine/go-json-rest/rest

# RUN mkdir -p ~/go/config
RUN mkdir -p /usr/local/go/src/bitbucket.org/just1nux/sonarrsmacker-go
COPY ./ /usr/local/go/src/bitbucket.org/just1nux/sonarrsmacker-go
WORKDIR /usr/local/go/src/bitbucket.org/just1nux/sonarrsmacker-go

EXPOSE 3030
